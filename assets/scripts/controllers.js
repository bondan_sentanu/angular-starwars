var swControllers = angular.module('swControllers', []);

swControllers.controller('mainCtrl', ['$scope', '$location', 
  function ($scope, $location) {
		$scope.isActive = function (viewLocation) {						
		     return (viewLocation === '#/'+$location.path().split("/")[1]);
		}
  }
]);

/* HOME CONTROLLER, FOR MAIN LAYOUT */
swControllers.controller('homeCtrl', ['$scope',
  function ($scope) {    
    $scope.tagline = 'May the force be with you!';
  }
]);

/* LOADER WHEN CHANGE PAGE IN MAIN LAYOUT */
swControllers.run(['$rootScope',function($rootScope){

    $rootScope.stateIsLoading = false;
    $rootScope.$on('$routeChangeStart', function() {
        $rootScope.stateIsLoading = true;
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $rootScope.stateIsLoading = false;
    });  
}]);

/* PEOPLE CONTROLLER */
swControllers.controller('peopleCtrl', ['$scope', '$routeParams', '$http', 
  function($scope, $routeParams, $http) {
  	$scope.url = "http://swapi.co/api/people/";
	$scope.loading = false; // Block fetching until the AJAX call returns
	$scope.people = [];

	$scope.getMore = function() {
		if ($scope.url !== null && !$scope.loading) {
			$scope.loading = true;	
			$http.get($scope.url)
			    .then(function(response) {			    	
			    	$scope.url = response.data.next;			    	
			        $scope.peoplePerPage = response.data.results;

			        // get the id of people and store it to the object
					// from http://swapi.co/api/people/1/ to 1
					for (var i=0; i<$scope.peoplePerPage.length;i++) {
						var peopleUrl = $scope.peoplePerPage[i].url;
						var peopleId = peopleUrl.split("/")[5];

						$scope.peoplePerPage[i].peopleId = peopleId;
					}

					// INFINITE SCROLL CONCAT
					$scope.people = $scope.people.concat($scope.peoplePerPage);					
			    }).finally(function() {
			    	$scope.loading = false;
				});
		}
	}
    
  }
]);

/* PEOPLE DETAIL CONTROLLER */
swControllers.controller('peopleDetailCtrl', ['$scope', '$routeParams', '$http',
  function($scope, $routeParams, $http) {
    $scope.peopleId = $routeParams.peopleId;
    var url = "http://swapi.co/api/people/" + $scope.peopleId + "/";

    $scope.loading = true;
    $http.get(url)
	    .then(function(response) {
	        $scope.person = response.data;
	    })
	    .finally(function() {
		    // called no matter success or failure
		    $scope.loading = false;
		});

  }
]);

/* PLANET LIST CONTROLLER */
swControllers.controller('planetsCtrl', ['$scope', '$routeParams', '$http', 
  function($scope, $routeParams, $http) {
	$scope.url = "http://swapi.co/api/planets/";
	$scope.loading = false; // Block fetching until the AJAX call returns
    $scope.planets = [];

	$scope.getMore = function() {
		if ($scope.url !== null && !$scope.loading) {
			$scope.loading = true;
			$http.get($scope.url)
			    .then(function(response) {			      
			      $scope.url = response.data.next;
			      $scope.planets = $scope.planets.concat(response.data.results);     
			       	      	      
			 	})
			 	.finally(function() {	 	  
				  // called no matter success or failure
				  $scope.loading = false;
				});
		}    	    	    
	}
			
  }
]);

/* FILM LIST CONTROLLER */
swControllers.controller('filmsCtrl', ['$scope', '$routeParams', '$http',
  function($scope, $routeParams, $http) {

	$scope.films = [];	
	$scope.url = "http://swapi.co/api/films/";
	$scope.loading = false;	

	$scope.getMore = function() {
		if ($scope.url !== null && !$scope.loading) {
		    $scope.loading = true; // Block fetching until the AJAX call returns
		    $http.get($scope.url)
			    .then(function(response) {			      
			      $scope.url = response.data.next;
			      $scope.films = $scope.films.concat(response.data.results);
			 	})
			 	.finally(function() {
				  // called no matter success or failure
				  $scope.loading = false;
				});
		}
	}

  }
]);