var swApp = angular.module('swApp', [
  'ngRoute',
  'swControllers',
  'tagged.directives.infiniteScroll'
]);

swApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'homeCtrl'
      }).
      when('/people', {
        templateUrl: 'partials/people-list.html',
        controller: 'peopleCtrl'
      }).
      when('/people/:peopleId', {
        templateUrl: 'partials/people-detail.html',
        controller: 'peopleDetailCtrl'
      }).
      when('/planets', {
        templateUrl: 'partials/planets-list.html',
        controller: 'planetsCtrl',        
      }).
      when('/planet/:planetId', {
        templateUrl: 'partials/planet-detail.html',
        controller: 'planetDetailCtrl'
      }).
      when('/films', {
        templateUrl: 'partials/films.html',
        controller: 'filmsCtrl'
      }).
      when('/about', {
        templateUrl: 'partials/about.html'
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]);